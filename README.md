# DevOps Onepager

Onepager einer fiktiven Firma für das Wahlpflichtmodul DevOps.
Folgende Schritte sind notwendig um sich den Onepager anzusehen:

- Repository herunterladen
- `index.html` öffnen in einem beliebigen Browser

# Vorgehensweise

Im folgenden werden kurz die Vorgehensweise bei dem Onepager erläutert.

Die Aufgabe war, ein Onepager für eine fiktive Firma zu entwickeln, bei dem einige Anforderungen gegeben waren: Die Firmenphilosophie, die Referenzen der Firma (bzw. Partner), das Team und der Kontakt zu der Firma sollten beschrieben werden.
Um den Onepager etwas interessanter zu gestalten, wurde entschieden eine Firma oder Organisation von einem science fiction Film zu nehmen. In diesem Fall ist es das Imperium von der Filmserie "Star Wars". Das grobe Gerüst der Webseite wurde von Bootstrap genommen (bei dem Scrollspy und ähnliches schon vorhanden war) und anschließend mit Inhalten ergänzt und editiert. 

- Firmenphilosophie: Das Ziel des Imperiums ist es, die komplette Herrschaft über die Galaxis zu erreichen. Das wurde auf der Webseite anhand von propaganda-ähnlichen Inhalten erreicht, bei denen das Imperium und seine Ziele erläutert werden.

- Referenzen: Da das Imperium generell keine Alliierten hat, wurden die Referenzen anhand von Kopfgeldjäger realisiert. Diese wurden verwendet, um möglichst schnell und einfach die Gegner des Imperiums auszuschalten.

- Team: Als Team wurden die wichtigsten Mitglieder des Imperiums aufgelistet.

- Kontakt: Der Kontaktteil der Seite ist vorhanden, die Inhalte wurden jedoch frei ausgedacht.

Es wurden noch ein paar zusätzliche Inhalte hinzugefügt, wie zum Beispiel die unterschiedlichen Rollen in der Firma (bzw. dem Imperium). Das meiste an css und javascript code wurde von dem Bootstrap theme übernommen, bis auf wenige Ergänzungen.
